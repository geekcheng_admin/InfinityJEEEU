package test.javaforever.infinity.complexverb;

import org.javaforever.infinity.complexverb.Assign;
import org.javaforever.infinity.complexverb.ListMyActive;
import org.javaforever.infinity.complexverb.ListMyAvailableActive;
import org.javaforever.infinity.complexverb.Revoke;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Field;
import org.junit.Test;

public class ComplexVerbTest {
	@Test
	public void testAssign()  throws Exception{
		String dbPrefix = "my_";
		
		Domain master = new Domain();
		master.setStandardName("Person");
		master.setPlural("People");
		master.setDomainId(new Field("id","Long"));
		master.setDomainName(new Field("name","String"));
		master.setActive(new Field("active","Boolean"));
		master.setDbPrefix(dbPrefix);
		
		Domain slave= new Domain();
		slave.setStandardName("Permission");
		slave.setDomainId(new Field("id","Long"));
		slave.setDomainName(new Field("permissionName","String"));
		slave.setActive(new Field("active","Boolean"));
		slave.setDbPrefix(dbPrefix);
		
		Assign assign = new Assign(master,slave);
		
//		System.out.println("========================Dao Impl=================");
//		System.out.println(assign.generateDaoImplMethodStringWithSerial());
//		System.out.println("========================Dao Definintion===========");
//		System.out.println(assign.generateDaoMethodDefinitionString());
		System.out.println("========================ServiceImpl===========");
		System.out.println(assign.generateServiceImplMethodStringWithSerial());
//		System.out.println("========================Service===============");
//		System.out.println(assign.generateServiceMethodDefinitionString());
//		System.out.println("========================Facade===========");
//		System.out.println(assign.generateFacadeMethodString());
//		System.out.println("========================FacadeWithSerial===========");
//		System.out.println(assign.generateFacadeMethodStringWithSerial());
	}
	
	@Test
	public void testRevoke()  throws Exception{
		String dbPrefix = "my_";
		
		Domain master = new Domain();
		master.setStandardName("Person");
		master.setPlural("People");
		master.setDomainId(new Field("id","Long"));
		master.setDomainName(new Field("name","String"));
		master.setActive(new Field("active","Boolean"));
		master.setDbPrefix(dbPrefix);
		
		Domain slave= new Domain();
		slave.setStandardName("Permission");
		slave.setDomainId(new Field("id","Long"));
		slave.setDomainName(new Field("permissionName","String"));
		slave.setActive(new Field("active","Boolean"));
		slave.setDbPrefix(dbPrefix);
		
		Revoke revoke = new Revoke(master,slave);
		
//		System.out.println("========================Dao Impl=================");
//		System.out.println(revoke.generateDaoImplMethodStringWithSerial());
//		System.out.println("========================Dao Definintion===========");
//		System.out.println(revoke.generateDaoMethodDefinitionString());
		System.out.println("========================ServiceImpl===========");
		System.out.println(revoke.generateServiceImplMethodStringWithSerial());
//		System.out.println("========================Service===============");
//		System.out.println(revoke.generateServiceMethodDefinitionString());
//		System.out.println("========================Facade===========");
//		System.out.println(revoke.generateFacadeMethodString());
//		System.out.println("========================FacadeWithSerial===========");
//		System.out.println(revoke.generateFacadeMethodStringWithSerial());
	}
	
	@Test
	public void testListMyActive()  throws Exception{
		String dbPrefix = "my_";
		
		Domain master = new Domain();
		master.setStandardName("Person");
		master.setPlural("People");
		master.setDomainId(new Field("id","Long"));
		master.setDomainName(new Field("name","String"));
		master.setActive(new Field("active","Boolean"));
		master.setDbPrefix(dbPrefix);
		
		Domain slave= new Domain();
		slave.setStandardName("Permission");
		slave.setDomainId(new Field("id","Long"));
		slave.setDomainName(new Field("permissionName","String"));
		slave.setActive(new Field("active","Boolean"));
		slave.setDbPrefix(dbPrefix);
		
		ListMyActive listMy = new ListMyActive(master,slave);
		
//		System.out.println("========================Dao Impl=================");
//		System.out.println(listMy.generateDaoImplMethodStringWithSerial());
//		System.out.println("========================Dao Definintion===========");
//		System.out.println(listMy.generateDaoMethodDefinitionString());
		System.out.println("========================ServiceImpl===========");
		System.out.println(listMy.generateServiceImplMethodStringWithSerial());
//		System.out.println("========================Service===============");
//		System.out.println(listMy.generateServiceMethodDefinitionString());
//		System.out.println("========================Facade===========");
//		System.out.println(listMy.generateFacadeMethodString());
//		System.out.println("========================FacadeWithSerial===========");
//		System.out.println(listMy.generateFacadeMethodStringWithSerial());
	}
	
	@Test
	public void testListMyAvailableActive()  throws Exception{
		String dbPrefix = "my_";
		
		Domain master = new Domain();
		master.setStandardName("Person");
		master.setPlural("People");
		master.setDomainId(new Field("id","Long"));
		master.setDomainName(new Field("name","String"));
		master.setActive(new Field("active","Boolean"));
		master.setDbPrefix(dbPrefix);
		
		Domain slave= new Domain();
		slave.setStandardName("Permission");
		slave.setDomainId(new Field("id","Long"));
		slave.setDomainName(new Field("permissionName","String"));
		slave.setActive(new Field("active","Boolean"));
		slave.setDbPrefix(dbPrefix);
		
		ListMyAvailableActive listMyAvailableActive = new ListMyAvailableActive(master,slave);
		
//		System.out.println("========================Dao Impl=================");
//		System.out.println(listMyAvailableActive.generateDaoImplMethodStringWithSerial());
//		System.out.println("========================Dao Definintion===========");
//		System.out.println(listMyAvailableActive.generateDaoMethodDefinitionString());
		System.out.println("========================ServiceImpl===========");
		System.out.println(listMyAvailableActive.generateServiceImplMethodStringWithSerial());
//		System.out.println("========================Service===============");
//		System.out.println(listMyAvailableActive.generateServiceMethodDefinitionString());
//		System.out.println("========================Facade===========");
//		System.out.println(listMyAvailableActive.generateFacadeMethodString());
//		System.out.println("========================FacadeWithSerial===========");
//		System.out.println(listMyAvailableActive.generateFacadeMethodStringWithSerial());
	}

}
