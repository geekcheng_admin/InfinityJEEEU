package org.javaforever.infinity.generator;

import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.JavascriptMethod;
import org.javaforever.infinity.domain.Signature;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.utils.StringUtil;

public class NamedS2SMJavascriptMethodGenerator {
	public static JavascriptMethod generateCheckAllMethod(Var checkAllBoxes , Var checkboxes,Var checkBoxClass){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("checkAll");
		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "if ($(\"#"+checkAllBoxes.getVarName()+"\").get(0).checked) {"));
		sl.add(new Statement(2000L,2, "var "+checkboxes.getVarName() +" = $(\"."+checkBoxClass.getVarName()+"\").each(function(i){"));
		sl.add(new Statement(3000L,3, "$(this)[0].checked = true;"));
		sl.add(new Statement(4000L,2, "});"));
		sl.add(new Statement(5000L,1, "} else {"));
		sl.add(new Statement(6000L,2, "var "+checkboxes.getVarName() +" = $(\"."+checkBoxClass.getVarName()+"\").each(function(i){"));
		sl.add(new Statement(7000L,3, "$(this)[0].checked = false;"));
		sl.add(new Statement(8000L,2, "});"));
		sl.add(new Statement(9000L,1, "}"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateGenerateDataRowsMethod(Domain domain,Var data,JavascriptMethod generateOpAllRow,Var datarow, Var domainIdClass,JavascriptMethod updateMethod, JavascriptMethod deleteMethod, JavascriptMethod softDeleteMethod, Var containerId){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("generateDataRows");
		Signature s = new Signature();
		s.setName(data.getVarName());
		s.setPosition(1);
		s.setType(new Type("var"));
		method.addSignature(s);
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var tableStr;"));
		sl.add(new Statement(2000L,1, "var opAllRowStr = "+generateOpAllRow.generateStandardMethodCallString()+";"));
		sl.add(new Statement(3000L,1, "$.each(data, function(i, item) {"));
		sl.add(new Statement(4000L,2, "tableStr += \"<tr id='"+datarow.getVarName()+"\"+i+\"'><td><input type='checkbox' name='"+domain.getDomainId().getLowerFirstFieldName()+"Checkbox' id='"+domain.getDomainId().getLowerFirstFieldName()+"Checkbox' class='"+domainIdClass.getVarName()+"' value='\" + item."+domain.getDomainId().getLowerFirstFieldName()+" + \"'/></td>\" +"));
		sl.add(new Statement(5000L,2, "\"<td><input type='hidden' name='"+domain.getDomainId().getLowerFirstFieldName()+"' id='"+domain.getDomainId().getLowerFirstFieldName()+"' value='\" + item."+domain.getDomainId().getLowerFirstFieldName()+" + \"'/><input type='text' name='"+domain.getDomainId().getLowerFirstFieldName()+"1' id='"+domain.getDomainId().getLowerFirstFieldName()+"1' size='8' value='\" + item."+domain.getDomainId().getLowerFirstFieldName()+" + \"'/></td>\" +"));
		long fieldSerial = 6000L;
		for (Field f: domain.getFieldsWithoutId()) {
			sl.add(new Statement(fieldSerial,2, "\"<td><input type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' size='8' value='\" + item."+f.getLowerFirstFieldName()+" + \"'/></td>\" +"));
			fieldSerial += 1000L;
		}
		sl.add(new Statement(fieldSerial,2, "\"<td><input type='button' value='Edit' onclick='"+updateMethod.getLowerFirstMethodName()+"(\"+i+\")'/><input type='button' value='Delete' onclick='"+deleteMethod.getLowerFirstMethodName()+"(\"+item."+domain.getDomainId().getLowerFirstFieldName()+"+\")'/><input type='button' value='Soft Delete' onclick='"+softDeleteMethod.getLowerFirstMethodName()+"(\"+item."+domain.getDomainId().getLowerFirstFieldName()+"+\")'/></td>\"+\"</tr>\";"));
		sl.add(new Statement(fieldSerial+1000L,1, "});"));
		sl.add(new Statement(fieldSerial+2000L,1, ""));	
		sl.add(new Statement(fieldSerial+3000L,1, "$(\"#"+containerId.getVarName()+"\").html(tableStr+opAllRowStr);"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateGenerateOpAllRowMethod(int colspan,Var opAllRowId, JavascriptMethod updateAllMethod, JavascriptMethod deleteAllMethod, JavascriptMethod softDeleteAllMethod){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("generateOpAllRow");
		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var rowstr = \"<tr id='opAllRow'> \"+"));
		sl.add(new Statement(2000L,2, "\"<td colspan='"+colspan+"' style='text-align:center'> \" +"));
		sl.add(new Statement(3000L,2, "\"<input type='button' value='Update All' onclick='"+updateAllMethod.getLowerFirstMethodName()+"()'/> &nbsp;&nbsp; \"+"));
		sl.add(new Statement(4000L,2, "\"<input type='button' value='Soft Delete All' onclick='"+softDeleteAllMethod.getLowerFirstMethodName()+"()'/> &nbsp;&nbsp; \"+"));
		sl.add(new Statement(5000L,2, "\"<input type='button' value='Delete All' onclick='"+deleteAllMethod.getLowerFirstMethodName()+"()'/></td>\"+"));
		sl.add(new Statement(6000L,2, "\"</tr>\";"));
		sl.add(new Statement(7000L,1, "return rowstr;"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateSearchByNameMethod(Domain domain,Var searchForm, Var myDomainNameField,JavascriptMethod generateDataRowsMethod){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("search"+domain.getPlural()+"By"+domain.getDomainName().getCapFirstFieldName());
		StatementList sl = new StatementList();
		
		sl.add(new Statement(1000L,1, "$.ajax({"));
		sl.add(new Statement(2000L,2, "type: \"post\","));
		sl.add(new Statement(3000L,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/search"+domain.getPlural()+"By"+domain.getDomainName().getCapFirstFieldName()+"\","));
		sl.add(new Statement(4000L,2, "data: {"));
		sl.add(new Statement(5000L,3,  "\""+domain.getDomainName().getLowerFirstFieldName()+"\":$(\"#"+searchForm.getVarName()+"\").find(\"#"+myDomainNameField.getVarName()+"\").val()"));
		sl.add(new Statement(6000L,2, "},"));
		sl.add(new Statement(7000L,2, "dataType: 'json',"));
		sl.add(new Statement(8000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(9000L,3, "if (data.success) "+generateDataRowsMethod.getStandardName()+"(data.data)"));  
		sl.add(new Statement(10000L,2, "},"));
		sl.add(new Statement(11000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(12000L,2, "},"));
		sl.add(new Statement(13000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(14000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(15000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(16000L,2, "}"));
		sl.add(new Statement(17000L,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateListAllByPageMethod(Domain domain,Var pagesize, Var pagenum,Var last,Var jumppagenum1, Var jumppagenum2, Var pagecount, JavascriptMethod generateDataRowsMethod){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("listAll"+domain.getPlural()+"ByPage");
		Signature s1 = new Signature();
		s1.setName(pagesize.getVarName());
		s1.setPosition(1);
		s1.setType(new Type("var"));
		Signature s2 = new Signature();
		s2.setName(pagenum.getVarName());
		s2.setPosition(2);
		Signature s3 = new Signature();
		s3.setName(last.getVarName());
		s3.setPosition(3);
		s2.setType(new Type("var"));
		method.addSignature(s1);
		method.addSignature(s2);
		method.addSignature(s3);
		
		StatementList sl = new StatementList();
		
		sl.add(new Statement(200L,1, "var myurl =\"../facade/"+domain.getLowerFirstDomainName()+"Facade/listAll"+domain.getPlural()+"ByPage\";"));
		sl.add(new Statement(1000L,1, "if ("+pagesize.getVarName()+" == undefined || "+pagesize.getVarName()+" == null || "+pagesize.getVarName()+" <= 0) "+pagesize.getVarName()+"=10;"));
		sl.add(new Statement(2000L,1, "if ("+pagenum.getVarName()+" == undefined || "+pagenum.getVarName()+" == null || "+pagenum.getVarName()+" <= 0) "+pagenum.getVarName()+"=1;"));		
		sl.add(new Statement(2200L,1, "if ("+last.getVarName()+" == undefined || "+last.getVarName()+" == null ) "+last.getVarName()+"=false;"));
		sl.add(new Statement(2400L,1, "myurl += \"/\"+"+pagenum.getVarName()+"+\"/\"+"+pagesize.getVarName()+";"));
		sl.add(new Statement(2600L,1, "myurl += \"/\"+"+last.getVarName()+";"));
		sl.add(new Statement(3000L,1, "$.ajax({"));
		sl.add(new Statement(4000L,2, "type: \"get\","));
		sl.add(new Statement(5000L,2, "url: myurl,"));
		sl.add(new Statement(10000L,2, "dataType: 'json',"));
		sl.add(new Statement(11000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(12000L,3, "if (data.success) {"));
		sl.add(new Statement(12100L,4, generateDataRowsMethod.getStandardName()+"(data.data);"));
		sl.add(new Statement(12200L,4, "$(\"#"+pagenum.getVarName()+"\").val(data."+pagenum.getVarName()+");"));
		sl.add(new Statement(12300L,4, "$(\"#"+jumppagenum1.getVarName()+"\").val(data."+pagenum.getVarName()+");"));
		sl.add(new Statement(12400L,4, "$(\"#"+jumppagenum2.getVarName()+"\").val(data."+pagenum.getVarName()+");"));
		sl.add(new Statement(12500L,4, "$(\"#"+pagecount.getVarName()+"\").val(data."+pagecount.getVarName()+");"));
		sl.add(new Statement(12600L,4, "$(\"#"+pagesize.getVarName()+"\").val(data."+pagesize.getVarName()+");"));
		sl.add(new Statement(13000L,3, "}"));
		sl.add(new Statement(13000L,2, "},"));
		sl.add(new Statement(14000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(15000L,2, "},"));
		sl.add(new Statement(16000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(17000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(18000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(19000L,2, "}"));
		sl.add(new Statement(20000L,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateAddMethod(Domain domain,Var addDomainRow,Var pagesize, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("add"+domain.getCapFirstDomainName());
		StatementList sl = new StatementList();
		
		sl.add(new Statement(1000L,1, "$.ajax({"));
		sl.add(new Statement(2000L,2, "type: \"post\","));
		sl.add(new Statement(3000L,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/add"+domain.getCapFirstDomainName()+"\","));
		sl.add(new Statement(4000L,2, "data: JSON.stringify({"));
		long rowIndex = 5000L;
		for (Field f : domain.getFieldsWithoutId()){
			if (f.getFieldType().equals("int")||f.getFieldType().equals("Integer")||f.getFieldType().equals("long")||f.getFieldType().equals("Long")){
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":parseInt($(\"#"+addDomainRow.getVarName()+"\").find(\"#"+f.getLowerFirstFieldName() +"\").val()),"));
			}else if (f.getFieldType().equalsIgnoreCase("float")||f.getFieldType().equalsIgnoreCase("double")||f.getFieldType().equalsIgnoreCase("decimal")||f.getFieldType().equals("BigDecimal")){
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":parseFloat($(\"#"+addDomainRow.getVarName()+"\").find(\"#"+f.getLowerFirstFieldName() +"\").val()),"));
			}else if (f.getFieldType().equalsIgnoreCase("boolean")){
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":parseBoolean($(\"#"+addDomainRow.getVarName()+"\").find(\"#"+f.getLowerFirstFieldName() +"\").val()),"));
			}else {
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":$(\"#"+addDomainRow.getVarName()+"\").find(\"#"+f.getLowerFirstFieldName() +"\").val(),"));
			}	
			rowIndex += 1000L;
		}
		sl.add(new Statement(rowIndex,2, "}),"));
		sl.add(new Statement(rowIndex+200L,2,"contentType:\"application/json;charset=UTF-8\","));
		sl.add(new Statement(rowIndex+400L,2,"async:false,"));
		sl.add(new Statement(rowIndex + 1000L,2, "dataType: 'json',"));
		sl.add(new Statement(rowIndex + 2000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(rowIndex + 3000L,3, "if (data.success) "+listAllByPage.getStandardName()+"("+pagesize.getVarName()+",1,true);"));
		rowIndex += 4000L;
		for (Field f : domain.getFieldsWithoutId()){
			sl.add(new Statement(rowIndex, 4, "$(\"#"+addDomainRow.getVarName()+"\").find(\"#"+f.getLowerFirstFieldName() +"\").val(\"\");"));
			rowIndex += 1000L;
		}
		sl.add(new Statement(rowIndex,3, "},"));
		sl.add(new Statement(rowIndex + 1000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(rowIndex + 2000L,2, "},"));
		sl.add(new Statement(rowIndex + 3000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(rowIndex + 4000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(rowIndex + 5000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(rowIndex + 6000L,2, "}"));
		sl.add(new Statement(rowIndex + 7000L,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	

	public static JavascriptMethod generateDeleteMethod(Domain domain,Var domainId,Var pagesize, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("delete"+domain.getCapFirstDomainName());
		Signature did = new Signature(1,domainId.getVarName(),"var");
		method.addSignature(did);
		StatementList sl = new StatementList();
		
		sl.add(new Statement(1000L,1, "$.ajax({"));
		sl.add(new Statement(2000L,2, "type: \"get\","));
		sl.add(new Statement(3000L,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/delete"+domain.getCapFirstDomainName()+"/\"+"+domainId.getVarName()+","));
		sl.add(new Statement(7000L,2, "dataType: 'json',"));
		sl.add(new Statement(8000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(9000L,3, "if (data.success) "+listAllByPage.getStandardName()+"("+pagesize.getVarName()+",1);"));
		sl.add(new Statement(10000L,2, "},"));		
		sl.add(new Statement(11000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(12000L,2, "},"));
		sl.add(new Statement(13000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(14000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(15000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(16000L,2, "}"));
		sl.add(new Statement(17000L,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateSoftDeleteMethod(Domain domain,Var domainId,Var pagesize, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("softDelete"+domain.getCapFirstDomainName());
		Signature did = new Signature(1,domainId.getVarName(),"var");
		method.addSignature(did);
		StatementList sl = new StatementList();
		
		sl.add(new Statement(1000L,1, "$.ajax({"));
		sl.add(new Statement(2000L,2, "type: \"get\","));
		sl.add(new Statement(3000L,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/softDelete"+domain.getCapFirstDomainName()+"/\"+"+domainId.getVarName()+","));
		sl.add(new Statement(7000L,2, "dataType: 'json',"));
		sl.add(new Statement(8000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(9000L,3, "if (data.success) "+listAllByPage.getStandardName()+"("+pagesize.getVarName()+",1);"));
		sl.add(new Statement(10000L,2, "},"));		
		sl.add(new Statement(11000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(12000L,2, "},"));
		sl.add(new Statement(13000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(14000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(15000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(16000L,2, "}"));
		sl.add(new Statement(17000L,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateDeleteAllMethod(Domain domain, Var domainCheckboxGroup, Var checkAllBox, Var pagesize, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("deleteAll"+domain.getPlural());
		StatementList sl = new StatementList();
		
		sl.add(new Statement(1000L,1, "var idArr=$(\"input[name='"+domainCheckboxGroup.getVarName()+"']:checked\").val([]);"));  
		sl.add(new Statement(2000L,1, "var ids = \"\";"));
		sl.add(new Statement(3000L,1, "for(var i=0;i<idArr.length;i++){"));  
		sl.add(new Statement(4000L,2, "ids += idArr[i].value;"));
		sl.add(new Statement(5000L,2, "if (i < idArr.length-1) ids += \",\";"));  
		sl.add(new Statement(6000L,1, "}"));		
		sl.add(new Statement(7000L,1, "$.ajax({"));
		sl.add(new Statement(8000L,2, "type: \"post\","));
		sl.add(new Statement(9000L,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/deleteAll"+domain.getPlural()+"\","));
		sl.add(new Statement(10000L,2, "data: {"));
		sl.add(new Statement(11000L,3, "ids:ids"));
		sl.add(new Statement(12000L,2, "},"));
		sl.add(new Statement(13000L,2, "dataType: 'json',"));
		sl.add(new Statement(14000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(15000L,3, "if (data.success) "+listAllByPage.getStandardName()+"("+pagesize.getVarName()+",1);"));
		sl.add(new Statement(16000L,2, "},"));		
		sl.add(new Statement(17000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(18000L,2, "},"));
		sl.add(new Statement(19000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(20000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(21000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(22000L,2, "}"));
		sl.add(new Statement(23000L,1, "});"));
		sl.add(new Statement(24000L,1, "$(\"input[name='"+checkAllBox.getVarName()+"']\").get(0).checked = false;"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateSoftDeleteAllMethod(Domain domain, Var domainCheckboxGroup, Var checkAllBox, Var pagesize, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("softDeleteAll"+domain.getPlural());
		StatementList sl = new StatementList();
		
		sl.add(new Statement(1000L,1, "var idArr=$(\"input[name='"+domainCheckboxGroup.getVarName()+"']:checked\").val([]);"));  
		sl.add(new Statement(2000L,1, "var ids = \"\";"));
		sl.add(new Statement(3000L,1, "for(var i=0;i<idArr.length;i++){"));  
		sl.add(new Statement(4000L,2, "ids += idArr[i].value;"));
		sl.add(new Statement(5000L,2, "if (i < idArr.length-1) ids += \",\";"));  
		sl.add(new Statement(6000L,1, "}"));		
		sl.add(new Statement(7000L,1, "$.ajax({"));
		sl.add(new Statement(8000L,2, "type: \"post\","));
		sl.add(new Statement(9000L,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/softDeleteAll"+domain.getPlural()+"\","));
		sl.add(new Statement(10000L,2, "data: {"));
		sl.add(new Statement(11000L,3, "ids:ids"));
		sl.add(new Statement(12000L,2, "},"));
		sl.add(new Statement(13000L,2, "dataType: 'json',"));
		sl.add(new Statement(14000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(15000L,3, "if (data.success) "+listAllByPage.getStandardName()+"("+pagesize.getVarName()+",1);"));
		sl.add(new Statement(16000L,2, "},"));		
		sl.add(new Statement(17000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(18000L,2, "},"));
		sl.add(new Statement(19000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(20000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(21000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(22000L,2, "}"));
		sl.add(new Statement(23000L,1, "});"));
		sl.add(new Statement(24000L,1, "$(\"input[name='"+checkAllBox.getVarName()+"']\").get(0).checked = false;"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateUpdateMethod(Domain domain,Var domainForm, Var dataRow,Var pagesize, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("update"+domain.getCapFirstDomainName());
		Signature myindex = new Signature();
		myindex.setName("myindex");
		myindex.setPosition(1);
		myindex.setType(new Type("var"));
		method.addSignature(myindex);
		StatementList sl = new StatementList();
		
		sl.add(new Statement(1000L,1, "$.ajax({"));
		sl.add(new Statement(2000L,2, "type: \"post\","));
		sl.add(new Statement(3000L,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/update"+domain.getCapFirstDomainName()+"/\"+"+domain.getDomainId().getLowerFirstFieldName()+","));
		sl.add(new Statement(4000L,2, "data: JSON.stringify({"));
		Field idField = domain.getDomainId();
		if (idField.getFieldType().equals("int")||idField.getFieldType().equals("Integer")||idField.getFieldType().equals("long")||idField.getFieldType().equals("Long")){
			sl.add(new Statement(5000L, 3,"\""+domain.getDomainId().getLowerFirstFieldName()+ "\":parseInt($(\"#"+domainForm.getVarName()+"\").find(\"#"+dataRow.getVarName()+"\"+"+myindex.getName()+").find(\"#"+domain.getDomainId().getLowerFirstFieldName() +"\").val()),"));
		}else {
			sl.add(new Statement(5000L, 3,"\""+domain.getDomainId().getLowerFirstFieldName()+ "\":$(\"#"+domainForm.getVarName()+"\").find(\"#"+dataRow.getVarName()+"\"+"+myindex.getName()+").find(\"#"+domain.getDomainId().getLowerFirstFieldName() +"\").val(),"));
		}	
		long rowIndex = 6000L;
		for (Field f : domain.getFieldsWithoutId()){
			if (f.getFieldType().equals("int")||f.getFieldType().equals("Integer")||f.getFieldType().equals("long")||f.getFieldType().equals("Long")){
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":parseInt($(\"#"+domainForm.getVarName()+"\").find(\"#"+dataRow.getVarName()+"\"+"+myindex.getName()+").find(\"#"+f.getLowerFirstFieldName() +"\").val()),"));
			}else if (f.getFieldType().equalsIgnoreCase("float")||f.getFieldType().equalsIgnoreCase("double")||f.getFieldType().equalsIgnoreCase("decimal")||f.getFieldType().equals("BigDecimal")){
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":parseFloat($(\"#"+domainForm.getVarName()+"\").find(\"#"+dataRow.getVarName()+"\"+"+myindex.getName()+").find(\"#"+f.getLowerFirstFieldName() +"\").val()),"));
			}else if (f.getFieldType().equalsIgnoreCase("boolean")){
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":parseBoolean($(\"#"+domainForm.getVarName()+"\").find(\"#"+dataRow.getVarName()+"\"+"+myindex.getName()+").find(\"#"+f.getLowerFirstFieldName() +"\").val()),"));
			}else {
				sl.add(new Statement(rowIndex, 3,"\""+f.getLowerFirstFieldName() + "\":$(\"#"+domainForm.getVarName()+"\").find(\"#"+dataRow.getVarName()+"\"+"+myindex.getName()+").find(\"#"+f.getLowerFirstFieldName() +"\").val(),"));
			}			
			rowIndex += 1000L;
		}
		sl.add(new Statement(rowIndex,2, "}),"));
		sl.add(new Statement(rowIndex+200L,2,"contentType:\"application/json;charset=UTF-8\","));
		sl.add(new Statement(rowIndex+400L,2,"async:false,"));
		sl.add(new Statement(rowIndex + 1000L,2, "dataType: 'json',"));
		sl.add(new Statement(rowIndex + 2000L,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(rowIndex + 3000L,3, "if (data.success) "+listAllByPage.getStandardName()+"("+pagesize.getVarName()+",1);"));
		sl.add(new Statement(rowIndex + 4000L,2, "},"));
		sl.add(new Statement(rowIndex + 5000L,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(rowIndex + 6000L,2, "},"));
		sl.add(new Statement(rowIndex + 7000L,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(rowIndex + 8000L,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(rowIndex + 9000L,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(rowIndex + 10000L,2, "}"));
		sl.add(new Statement(rowIndex + 11000L,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateUpdateAllMethod(Domain domain,Var domainForm, Var dataRow,JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("updateAll"+domain.getPlural());

		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "alert(\"update all\");"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateJump1Method(Var pagesize, Var pagenum,Var jumppagenum1,JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("jump1");

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var "+pagesize.getVarName() + " = $(\"#"+pagesize.getVarName() +"\").val();"));
		sl.add(new Statement(2000L,1, "var "+pagenum.getVarName() + " = $(\"#"+jumppagenum1.getVarName() +"\").val();"));
		sl.add(new Statement(3000L,1, StringUtil.lowerFirst(listAllByPage.getStandardName())+"("+pagesize.getVarName()+","+pagenum.getVarName() +")"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateJump2Method(Var pagesize, Var pagenum,Var jumppagenum2,JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("jump2");

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var "+pagesize.getVarName() + " = $(\"#"+pagesize.getVarName() +"\").val();"));
		sl.add(new Statement(2000L,1, "var "+pagenum.getVarName() + " = $(\"#"+jumppagenum2.getVarName() +"\").val();"));
		sl.add(new Statement(3000L,1, StringUtil.lowerFirst(listAllByPage.getStandardName())+"("+pagesize.getVarName()+","+pagenum.getVarName() +")"));
		method.setMethodStatementList(sl);
		return method;
	}

	public static JavascriptMethod generateFirstMethod(Var pagesize, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("first");

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var "+pagesize.getVarName() + " = $(\"#"+pagesize.getVarName() +"\").val();"));
		sl.add(new Statement(2000L,1, StringUtil.lowerFirst(listAllByPage.getStandardName())+"("+pagesize.getVarName()+",1)"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generatePrevMethod(Var pagesize,Var pagenum, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("prev");

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var "+pagesize.getVarName() + " = $(\"#"+pagesize.getVarName() +"\").val();"));
		sl.add(new Statement(2000L,1, "var "+pagenum.getVarName() + " = parseInt($(\"#"+pagenum.getVarName() +"\").val()) - 1;"));
		sl.add(new Statement(3000L,1, StringUtil.lowerFirst(listAllByPage.getStandardName())+"("+pagesize.getVarName()+","+pagenum.getVarName() +")"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateNextMethod(Var pagesize,Var pagenum, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("next");

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var "+pagesize.getVarName() + " = $(\"#"+pagesize.getVarName() +"\").val();"));
		sl.add(new Statement(2000L,1, "var "+pagenum.getVarName() + " = parseInt($(\"#"+pagenum.getVarName() +"\").val()) + 1;"));
		sl.add(new Statement(3000L,1, StringUtil.lowerFirst(listAllByPage.getStandardName())+"("+pagesize.getVarName()+","+pagenum.getVarName() +")"));
		method.setMethodStatementList(sl);
		return method;
	}

	public static JavascriptMethod generateLastMethod(Var pagesize,Var pagecount, JavascriptMethod listAllByPage){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("last");

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var "+pagesize.getVarName() + " = $(\"#"+pagesize.getVarName() +"\").val();"));
		sl.add(new Statement(2000L,1, "var "+pagecount.getVarName() + " = $(\"#"+pagecount.getVarName() +"\").val();"));
		sl.add(new Statement(3000L,1, StringUtil.lowerFirst(listAllByPage.getStandardName())+"("+pagesize.getVarName()+","+pagecount.getVarName() +")"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateParseBooleanMethod(){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("parseBoolean");
		method.addSignature(new Signature(1,"val","var"));

		StatementList sl = new StatementList();
		sl.add(new Statement(500L,1, "if (val == undefined|| val == null) return false;"));
		sl.add(new Statement(1000L,1, "var isInt = /^(-}+)?\\d+$/.test(val);"));
		sl.add(new Statement(2000L,1, "if (isInt&& val == \"0\") return false;"));
		sl.add(new Statement(3000L,1, "else if (isInt&& val == \"1\") return true;"));	
		sl.add(new Statement(4000L,1, "else if (val.toLowerCase() == \"false\") return false;"));	
		sl.add(new Statement(5000L,1, "else if (val.toLowerCase() == \"true\") return true;"));
		sl.add(new Statement(6000L,1, "else if (val.toLowerCase() == \"f\") return false;"));
		sl.add(new Statement(7000L,1, "else if (val.toLowerCase() == \"t\") return true;"));
		sl.add(new Statement(8000L,1, "else if (val.toLowerCase() == \"n\") return false;"));
		sl.add(new Statement(9000L,1, "else if (val.toLowerCase() == \"y\") return true;"));
		sl.add(new Statement(10000L,1, "else if (val.toLowerCase() == \"no\") return false;"));
		sl.add(new Statement(11000L,1, "else if (val.toLowerCase() == \"yes\") return true;"));
		sl.add(new Statement(12000L,1, "else return \"\";"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateIsBlankMethod(){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("isBlank");
		method.addSignature(new Signature(1,"val","var"));

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "if (val == undefined|| val == null || val == \"\") return true;"));
		sl.add(new Statement(2000L,1, "else return false;"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateCheckMyRowMethod(Domain master,Domain slave,Var currentIndex){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("checkMyRow");
		method.addSignature(new Signature(1,"index","var"));
		method.addSignature(new Signature(2,"row","var"));

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "var "+master.getLowerFirstDomainName()+"Id;"));
		sl.add(new Statement(2000L,1, "if (row) {"));
		sl.add(new Statement(3000L,1, master.getLowerFirstDomainName()+"Id = row."+master.getDomainId().getLowerFirstFieldName()+";"));
		sl.add(new Statement(3500L,1,currentIndex.getVarName()+" = index;"));
		sl.add(new Statement(4000L,1, "}"));
		sl.add(new Statement(5000L,1, "$('#my"+slave.getCapFirstPlural()+"').datalist('load',{"));
		sl.add(new Statement(6000L,1, master.getLowerFirstDomainName()+"Id: "+master.getLowerFirstDomainName()+"Id"));
		sl.add(new Statement(7000L,1, "});"));
		sl.add(new Statement(8000L,1, "$('#myAvailable"+slave.getCapFirstPlural()+"').datalist('load',{"));
		sl.add(new Statement(9000L,1, master.getLowerFirstDomainName()+"Id: "+master.getLowerFirstDomainName()+"Id"));
		sl.add(new Statement(10000L,1, "});"));
		method.setMethodStatementList(sl);
		return method;
	}
	
	public static JavascriptMethod generateLoadSuccessMethod(Domain master,Var currentIndex){
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("loadSuccess");

		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,1, "$(\"#my"+master.getCapFirstDomainName()+"\").datalist(\"checkRow\","+currentIndex.getVarName()+");"));
		method.setMethodStatementList(sl);
		return method;
	}
}
